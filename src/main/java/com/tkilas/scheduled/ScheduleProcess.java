package com.tkilas.scheduled;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.TemporalType;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.tkilas.Utils.AESUtils;
import com.tkilas.Utils.Mailing_Utils;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasPromo;
import com.tkilas.model.Comment;
import com.tkilas.model.CommentPK;
import com.tkilas.model.Local;

@Service
public class ScheduleProcess {

	private static final Logger logger = LoggerFactory
			.getLogger(ScheduleProcess.class);

	@Autowired
	private SessionFactory factory;

	@Autowired
	private JavaMailSender mailSender;

	// Execute once every day at 10:00 AM
	@Scheduled(cron="0 0 10 * * ?")
	
	//@Scheduled(cron = "0/200 * * * * ?")
	@Transactional
	public void demoServiceMethod() {
		logger.debug("Empezamos el proceso de batch :: " + new Date());

		Session session = factory.openSession();

		Calendar cal = Calendar.getInstance();
		Date dateFin = cal.getTime();
		cal.add(Calendar.DAY_OF_YEAR, -1);
		Date dateIni = cal.getTime();
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateIniStr = sdf.format(dateIni);
		String dateFinStr = sdf.format(dateFin);

		
		
		Transaction t = session.beginTransaction();
		Query queryDisscount = null;
		Query queryProduct = null;
		
			queryDisscount = session
					.createQuery(
							"FROM ClientHasDiscount WHERE date BETWEEN '"+dateIniStr+"' AND  '"+dateFinStr+"'");
					
			queryProduct = session
					.createQuery(
							"FROM ClientHasPromo WHERE date BETWEEN '"+dateIniStr+"' AND  '"+dateFinStr+"'");
					

		
		
		// Query query =
		// session.createQuery("FROM ClientHasDiscount WHERE date < :dateFin").setParameter("dateFin",
		// dateFin);
		
		System.out.println("Wololo: "+dateFin);
		System.out.println("Wololo: "+dateIni);

		List<ClientHasDiscount> chdList = (List<ClientHasDiscount>) queryDisscount.list();
		List<ClientHasPromo> chpList = (List<ClientHasPromo>) queryProduct.list();
		t.commit();
		session.close();

		Iterator<ClientHasDiscount> itChd = chdList.iterator();
		Iterator<ClientHasPromo> itChp = chpList.iterator();

		Mailing_Utils mail_utils = new Mailing_Utils();
		int errors = 0;

		while (itChd.hasNext()) {
			ClientHasDiscount chd = itChd.next();
			String mailToSend = chd.getClient().getUser().getEmail();
			String nameOfClient = chd.getClient().getUser().getUser_name()
					+ " " + chd.getClient().getUser().getUser_surname();
			Date dateOfDisscount = chd.getId().getDate();
			String aux = new SimpleDateFormat("yyyy/MM/dd")
					.format(dateOfDisscount);
			String fingerPrint = AESUtils.SHA512(mailToSend + aux);
			Session sessionAux = factory.openSession();
			Transaction t2 = sessionAux.beginTransaction();

			String urlSurvey = "http://tkilas.com/survey?fingerprint="
					+ fingerPrint;
			System.out.println(urlSurvey + " " + urlSurvey);

			Query queryAux = sessionAux.createQuery(
					"FROM Local WHERE user_id_user = :localId").setParameter(
					"localId", chd.getId().getLocalId());
			Local local = ((Local) queryAux.uniqueResult());

			t2.commit();
			sessionAux.close();
			logger.debug("Enviar mail a: " + mailToSend
					+ " con nombre de cliente: " + nameOfClient
					+ " con fecha de consumo " + dateOfDisscount
					+ " para el local" + local.getLocal_name()
					+ " FINGERPRINT: " + fingerPrint);

			String localAddress = local.getAddress() + ", "
					+ local.getLocal_cp() + " " + local.getCity();

			String mailBoyd = mail_utils.constructHTMLFormatedMailForSurvey(
					nameOfClient, urlSurvey, local.getLocal_name(), aux,
					"http://tkilas.com"+local.getUrl_pic1(), localAddress);
			String mailSubject = nameOfClient
					+ ", tu opinión nos ayuda a mejorar";
			// JavaMailSender mailSender,String from, String to,String subject,
			// String body
			boolean returnValue = false;
			try {
				 returnValue = mail_utils.send_HTMLFormated_mail(
						mailSender, Mailing_Utils.INFO_MAIL, mailToSend,
						mailSubject, mailBoyd);

			} catch (Exception e) {
				continue;
			}
			// boolean returnValue=true;
			if (returnValue) {
				// Si ha ido bien el envio, persisto en BBDD
				logger.debug("Mail enviado correctamente, persisto");
				Session sessionAux2 = factory.openSession();
				Transaction t3 = sessionAux2.beginTransaction();
				Comment comment = new Comment();
				comment.setCommentBody("");
				comment.setFingerprint(fingerPrint);

				CommentPK id = new CommentPK();
				id.setClientUserIdUser(chd.getId().getClientId());
				id.setLocalUserIdUser(chd.getId().getLocalId());
				id.setPack_date(chd.getId().getDate());
				comment.setId(id);

				sessionAux2.save(comment);
				t3.commit();

				sessionAux2.close();

			} else {
				// Si no, no persisto para ahorrar espacio
				logger.debug("Mail no enviado, Finalizo");
				errors++;

			}

		}

		while (itChp.hasNext()) {
			ClientHasPromo chp = itChp.next();
			String mailToSend = chp.getClient().getUser().getEmail();
			String nameOfClient = chp.getClient().getUser().getUser_name()
					+ " " + chp.getClient().getUser().getUser_surname();
			Date dateOfDisscount = chp.getId().getDate();
			String aux = new SimpleDateFormat("yyyy/MM/dd")
					.format(dateOfDisscount);
			String fingerPrint = AESUtils.SHA512(mailToSend + aux);
			Session sessionAux = factory.openSession();
			Transaction t2 = sessionAux.beginTransaction();
			String urlSurvey = "http://tkilas.com/survey?fingerprint="
					+ fingerPrint;

			Query queryAux = sessionAux.createQuery(
					"FROM Local WHERE user_id_user = :localId").setParameter(
					"localId", chp.getId().getLocalId());
			Local local = ((Local) queryAux.uniqueResult());

			t2.commit();
			sessionAux.close();
			logger.debug("Enviar mail a: " + mailToSend
					+ " con nombre de cliente: " + nameOfClient
					+ " con fecha de consumo " + dateOfDisscount
					+ " para el local" + local.getLocal_name()
					+ " FINGERPRINT: " + fingerPrint);

			String localAddress = local.getAddress() + ", "
					+ local.getLocal_cp() + " " + local.getCity();

			String mailBoyd = mail_utils.constructHTMLFormatedMailForSurvey(
					nameOfClient, urlSurvey, local.getLocal_name(), aux,
					"http://tkilas.com"+local.getUrl_pic1(), localAddress);
			String mailSubject = nameOfClient
					+ ", tu opinión nos ayuda a mejorar";
			// JavaMailSender mailSender,String from, String to,String subject,
			// String body
			boolean returnValue = false;
			try {
				 returnValue = mail_utils.send_HTMLFormated_mail(
						mailSender, Mailing_Utils.INFO_MAIL, mailToSend,
						mailSubject, mailBoyd);
			} catch (Exception e) {
				continue;
			}

			// boolean returnValue=true;
			if (returnValue) {
				// Si ha ido bien el envio, persisto en BBDD
				logger.debug("Mail enviado correctamente, persisto");
				Session sessionAux2 = factory.openSession();
				Transaction t3 = sessionAux2.beginTransaction();
				Comment comment = new Comment();
				comment.setCommentBody("");
				comment.setFingerprint(fingerPrint);

				CommentPK id = new CommentPK();
				id.setClientUserIdUser(chp.getId().getClientId());
				id.setLocalUserIdUser(chp.getId().getLocalId());
				id.setPack_date(chp.getId().getDate());
				comment.setId(id);

				sessionAux2.save(comment);
				t3.commit();

				sessionAux2.close();

			} else {
				// Si no, no persisto para ahorrar espacio
				logger.debug("Mail no enviado, Finalizo");
				errors++;

			}

		}

		// Envio un correo para ver el status a mi mismo
		// JavaMailSender mailSender,String from, String to,String subject,
		// String body
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String b = "El informe concluyó con un total de: "
				+ chdList.size()
				+ " reservas, de las cuales han sido enviados mail correctamente a: "
				+ (chdList.size() - errors) + "\n Han habido errores en: "
				+ errors;

		try {
			mail_utils.send_HTMLFormated_mail(mailSender,
					Mailing_Utils.INFO_MAIL, "miguel.pereira@tkilas.com",
					"Informe de estado para: " + format.format(dateFin), b);

		} catch (Exception e) {
			System.out.println("Error al enviarme el mail a mi mismo");
		}

	}

}
