package com.tkilas.model;

import javax.persistence.Entity;


public class ComercialReportRow {

	private int localId;
	private String local_name;
	private String comercial;
	private String create_time;
	private int num_reservas;
	private int num_total_reservas;
	private double reservas_percentage;
	private double ingresos_descuentos_local_periodo;
	private double ingresos_producto_local_periodo;
	private double ingresos_local_periodo_total;
	private double ingresos_periodo_percentage_local;
	private double ingresos_periodo_total;
	private double ingresos_acumulado_local_periodo;
	private double ingresos_acumulado_local_total;
	private double ingresos_acumulado_local_percentage;

	public int getLocalId() {
		return localId;
	}

	public void setLocalId(int localId) {
		this.localId = localId;
	}

	public String getLocal_name() {
		return local_name;
	}

	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}

	public String getComercial() {
		return comercial;
	}

	public void setComercial(String comercial) {
		this.comercial = comercial;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public int getNum_reservas() {
		return num_reservas;
	}

	public void setNum_reservas(int num_reservas) {
		this.num_reservas = num_reservas;
	}

	public int getNum_total_reservas() {
		return num_total_reservas;
	}

	public void setNum_total_reservas(int num_total_reservas) {
		this.num_total_reservas = num_total_reservas;
	}

	public double getReservas_percentage() {
		return reservas_percentage;
	}

	public void setReservas_percentage(double reservas_percentage) {
		this.reservas_percentage = reservas_percentage;
	}

	public double getIngresos_descuentos_local_periodo() {
		return ingresos_descuentos_local_periodo;
	}

	public void setIngresos_descuentos_local_periodo(double ingresos_descuentos_local_periodo) {
		this.ingresos_descuentos_local_periodo = ingresos_descuentos_local_periodo;
	}

	public double getIngresos_producto_local_periodo() {
		return ingresos_producto_local_periodo;
	}

	public void setIngresos_producto_local_periodo(double ingresos_producto_local_periodo) {
		this.ingresos_producto_local_periodo = ingresos_producto_local_periodo;
	}

	public double getIngresos_local_periodo_total() {
		return ingresos_local_periodo_total;
	}

	public void setIngresos_local_periodo_total(double ingresos_local_periodo_total) {
		this.ingresos_local_periodo_total = ingresos_local_periodo_total;
	}

	public double getIngresos_periodo_percentage_local() {
		return ingresos_periodo_percentage_local;
	}

	public void setIngresos_periodo_percentage_local(double ingresos_periodo_percentage_local) {
		this.ingresos_periodo_percentage_local = ingresos_periodo_percentage_local;
	}

	public double getIngresos_periodo_total() {
		return ingresos_periodo_total;
	}

	public void setIngresos_periodo_total(double ingresos_periodo_total) {
		this.ingresos_periodo_total = ingresos_periodo_total;
	}

	public double getIngresos_acumulado_local_periodo() {
		return ingresos_acumulado_local_periodo;
	}

	public void setIngresos_acumulado_local_periodo(double ingresos_acumulado_local_periodo) {
		this.ingresos_acumulado_local_periodo = ingresos_acumulado_local_periodo;
	}

	public double getIngresos_acumulado_local_total() {
		return ingresos_acumulado_local_total;
	}

	public void setIngresos_acumulado_local_total(double ingresos_acumulado_local_total) {
		this.ingresos_acumulado_local_total = ingresos_acumulado_local_total;
	}

	public double getIngresos_acumulado_local_percentage() {
		return ingresos_acumulado_local_percentage;
	}

	public void setIngresos_acumulado_local_percentage(double ingresos_acumulado_local_percentage) {
		this.ingresos_acumulado_local_percentage = ingresos_acumulado_local_percentage;
	}

}
