package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.*;


import java.util.List;


/**
 * The persistent class for the client database table.
 * 
 */
@Entity
@Table(name = "client")
@NamedQuery(name="Client.findAll", query="SELECT c FROM Client c")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id_user", insertable=false,updatable=false)
	private int userIdUser;

	@Column(name="client_city")
	private String clientCity;

	@Column(name="client_cp")
	private String clientCp;

	@Column(name="client_profile_pic")
	private String clientProfilePic;

	@Column(name="client_sex")
	private int clientSex;

	@Column(name="client_SO")
	private int client_SO;

	@Column(name="device_uuid")
	private String deviceUuid;

	//bi-directional one-to-one association to User
	@OneToOne (cascade = CascadeType.ALL)
	@JoinColumn(name="user_id_user", insertable = false, updatable= false)
	private User user;
/*
	//bi-directional many-to-one association to ClientHasDiscount
	//@JsonIgnore
	@OneToMany(mappedBy="client",  fetch=FetchType.LAZY )
	private List<ClientHasDiscount> clientHasDiscounts;

	//bi-directional many-to-one association to ClientHasPromo
	@OneToMany(mappedBy="client")
	private List<ClientHasPromo> clientHasPromos;
*/
	//bi-directional many-to-one association to Comment
	@OneToMany(mappedBy="client")
	private List<Comment> comments;

	public Client() {
	}

	public int getUserIdUser() {
		return this.userIdUser;
	}

	public void setUserIdUser(int userIdUser) {
		this.userIdUser = userIdUser;
	}

	public String getClientCity() {
		return this.clientCity;
	}

	public void setClientCity(String clientCity) {
		this.clientCity = clientCity;
	}

	public String getClientCp() {
		return this.clientCp;
	}

	public void setClientCp(String clientCp) {
		this.clientCp = clientCp;
	}

	public String getClientProfilePic() {
		return this.clientProfilePic;
	}

	public void setClientProfilePic(String clientProfilePic) {
		this.clientProfilePic = clientProfilePic;
	}

	public int getClientSex() {
		return this.clientSex;
	}

	public void setClientSex(int clientSex) {
		this.clientSex = clientSex;
	}

	public int getClient_SO() {
		return this.client_SO;
	}

	public void setClient_SO(int client_SO) {
		this.client_SO = client_SO;
	}

	public String getDeviceUuid() {
		return this.deviceUuid;
	}

	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	/*
	@JsonBackReference
	public List<ClientHasDiscount> getClientHasDiscounts() {
		return this.clientHasDiscounts;
	}
	@JsonBackReference
	public void setClientHasDiscounts(List<ClientHasDiscount> clientHasDiscounts) {
		this.clientHasDiscounts = clientHasDiscounts;
	}

	
	public ClientHasDiscount addClientHasDiscount(ClientHasDiscount clientHasDiscount) {
		getClientHasDiscounts().add(clientHasDiscount);
		clientHasDiscount.setClient(this);

		return clientHasDiscount;
	}

	public ClientHasDiscount removeClientHasDiscount(ClientHasDiscount clientHasDiscount) {
		getClientHasDiscounts().remove(clientHasDiscount);
		clientHasDiscount.setClient(null);

		return clientHasDiscount;
	}

	public List<ClientHasPromo> getClientHasPromos() {
		return this.clientHasPromos;
	}

	public void setClientHasPromos(List<ClientHasPromo> clientHasPromos) {
		this.clientHasPromos = clientHasPromos;
	}

	public ClientHasPromo addClientHasPromo(ClientHasPromo clientHasPromo) {
		getClientHasPromos().add(clientHasPromo);
		clientHasPromo.setClient(this);

		return clientHasPromo;
	}

	public ClientHasPromo removeClientHasPromo(ClientHasPromo clientHasPromo) {
		getClientHasPromos().remove(clientHasPromo);
		clientHasPromo.setClient(null);

		return clientHasPromo;
	}
*/
	public List<Comment> getComments() {
		return this.comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Comment addComment(Comment comment) {
		getComments().add(comment);
		comment.setClient(this);

		return comment;
	}

	public Comment removeComment(Comment comment) {
		getComments().remove(comment);
		comment.setClient(null);

		return comment;
	}

}