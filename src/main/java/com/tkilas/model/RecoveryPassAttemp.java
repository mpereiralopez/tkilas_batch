package com.tkilas.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the recovery_pass_attemps database table.
 * 
 */
@Entity
@Table(name = "recovery_pass_attemps")

@NamedQuery(name="RecoveryPassAttemp.findAll", query="SELECT r FROM RecoveryPassAttemp r")
public class RecoveryPassAttemp implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="id_user")
	private int userIdUser;
		
	@Column(name="user_email")
	private String email;
	
	@Column(name="fingerprint")
	private String fingerprint;
	
	@Column(name="time_create")
	private Timestamp time_create;
	
	@Column(name="time_max")
	private Timestamp time_max;
	
	@Column(name="is_done")
	private boolean is_done;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserIdUser() {
		return userIdUser;
	}

	public void setUserIdUser(int userIdUser) {
		this.userIdUser = userIdUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFingerprint() {
		return fingerprint;
	}

	public void setFingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
	}

	public Timestamp getTime_create() {
		return time_create;
	}

	public void setTime_create(Timestamp time_create) {
		this.time_create = time_create;
	}
	
	public Timestamp getTime_max() {
		return time_max;
	}

	public void setTime_max(Timestamp time_max) {
		this.time_max = time_max;
	}

	public boolean isIs_done() {
		return is_done;
	}

	public void setIs_done(boolean is_done) {
		this.is_done = is_done;
	}

	
}
