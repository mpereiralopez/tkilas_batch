package com.tkilas.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;


public class Mailing_Utils {
	
	
	
	private static final Logger logger = LoggerFactory.getLogger(Mailing_Utils.class);

	public static final String INFO_MAIL = "info@tkilas.com";
	
	
	
	public boolean send_HTMLFormated_mail(JavaMailSender mailSender,String from, String to,String subject, String body) throws MailSendException, MessagingException {
		boolean aux = false;
		// Ahora hago testing para enviar correos
			MimeMessage message = mailSender.createMimeMessage();
			message.setSubject(subject);
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(from);
			helper.setTo(to);
			helper.setText(body, true);
			mailSender.send(message);
			aux = true;
		
		
		return aux;
	}
	
	

	
	public String constructHTMLFormatedMailForSurvey (String fullUserName, String fingerprint, String localName, String dateOfDisscount, String localPicUrl, String localAddress){
		StringBuilder body = new StringBuilder();

		//getClass().getResourceAsStream("surveyMailTemplate.html")
	
		    
		    Scanner scanner = new Scanner(getClass().getResourceAsStream("surveyMailTemplate.html"));


		    while(scanner.hasNextLine()){
		    	body.append(scanner.nextLine());
		     }
		    
		    scanner.close();

		    
	
		//#USERNAME#
		//#LOCALNAME#
		//"#LOCALURLPIC#"
		//#LOCALADDRESS#
		//#DATE#
		//#URLSURVEY#
		
		String bodyStringifier = body.toString();
		System.out.println(bodyStringifier.indexOf("#USERNAME#"));
		bodyStringifier = bodyStringifier.replaceAll("#USERNAME#", fullUserName);
		bodyStringifier = bodyStringifier.replaceAll("#LOCALNAME#", localName);
		bodyStringifier = bodyStringifier.replaceAll("#LOCALURLPIC#", localPicUrl);
		bodyStringifier = bodyStringifier.replaceAll("#LOCALADDRESS#", localAddress);
		bodyStringifier = bodyStringifier.replaceAll("#DATE#", dateOfDisscount);
		bodyStringifier = bodyStringifier.replaceAll("#URLSURVEY#", fingerprint);

		
		return bodyStringifier;
	}

}
