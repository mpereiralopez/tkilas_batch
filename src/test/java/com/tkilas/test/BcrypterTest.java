package com.tkilas.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.tkilas.Utils.Mailing_Utils;


public class BcrypterTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Mailing_Utils utils = new Mailing_Utils();
		
		Date date = Calendar.getInstance().getTime();
		//#USERNAME#
		String fullName = "Miguel Pereira López";
		//#LOCALNAME#
		String localName = "Casa Dolores";
		
		//"#LOCALURLPIC#"
		String localUrlPic ="urlLocal";
		
		//#LOCALADDRESS#
		String localAddress = "Calle de la Vitoria";
		
		SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy");

		//#DATE#
		String dateFormated = simpledateformat.format(date);
		
		//#URLSURVEY#
		String urlSurvey = "fingerprint";

		String body = utils.constructHTMLFormatedMailForSurvey(fullName, urlSurvey, localName, dateFormated, localUrlPic,localAddress);
		System.out.println(body);
	}

}
